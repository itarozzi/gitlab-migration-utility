# GitLab Migration Utility


# Description 

Copy a project, with its issues and labels, from a GitLab instance to another.

Because the moving issue API works only in the same GitLab instance, I created this script to copy projects from my own GitLab installation (on my VPS) to the gitlab.com server.

User API token for the source and destination GitLab instances are required.

## Usage
Define URL, TOKEN and destination group in the source code or as environment variables.

```
export GITLAB1_URL="https://my.gitlab.repo"
export GITLAB1_API_TOKEN="<my_token1_here>"

export GITLAB2_URL="https://gitlab.com"
export GITLAB2_API_TOKEN="<my_token1_here>"

export GITLAB2_URL_GROUP="mygroup/mysubgroup"
```


Execute the script and select the project to copy.


Once  the new project is created, you need to update the git remote repository in your local file system.

```
git remote rename origin old_origin
git remote add origin <url_of_new_repo>
git push -u  origin --all
git push --tags origin
```

### Credits

based on the move_issues.py code by Adam Charnock
https://gist.github.com/adamcharnock/44b27418ac0b1929f91f7c4c3b1bb86a
#!/usr/bin/env python3

import os
import gitlab

# Source GITLAB Server
#GITLAB1_URL = 'https://gitlab.com'
GITLAB1_URL = os.environ['GITLAB1_URL']
GITLAB1_API_TOKEN = os.environ['GITLAB1_API_TOKEN']


# Destination GITLAB Server
#GITLAB2_URL = 'https://gitlab.com'
GITLAB2_URL = os.environ['GITLAB2_URL']

#GITLAB2_URL_GROUP = 'mygroup/mysubgroup'
GITLAB2_URL_GROUP = os.environ['GITLAB2_URL_GROUP']

GITLAB2_API_TOKEN = os.environ['GITLAB2_API_TOKEN']


def main():
    print("Logging into gitlab and fetching a list of projects...")
    gl = gitlab.Gitlab(GITLAB1_URL, private_token=GITLAB1_API_TOKEN, ssl_verify=False)
    gl2 = gitlab.Gitlab(GITLAB2_URL, private_token=GITLAB2_API_TOKEN)

    projects_src = gl.projects.list(membership=1, all=True)

    print(f"Here are the projects we found in SRC server:")
    for i, project in enumerate(projects_src):
        print(f"    {i+1}. {project.name}")
    from_project_number = input(f"\nWhich project do you want to move issues FROM? Enter  1 - {len(projects_src)}: ")

    from_project = projects_src[int(from_project_number.strip()) - 1]

    print(f'Fetching issues for {from_project.name} (this can take a while)...')
    issues = from_project.issues.list(all=True)
    issues = sorted(issues, key=lambda i: i.iid)

    print(f'Fetching labels for {from_project.name}...')
    labels = from_project.labels.list(all=True)

    print(f'Will create new project called:  {from_project.name}')
    print(f'destination group:  {GITLAB2_URL_GROUP}')
    print(f'Will move {len(issues)} issues from {from_project.name}')
    print(f'Will also create {len(labels)} labels in new  project ')
    cont = input('Continue [y/n]? ')
    if cont.strip().lower() != 'y':
        return

    print('Creating project')
    group_id = gl2.groups.list(search=GITLAB2_URL_GROUP)[0].id
    to_project = gl2.projects.create({'name': from_project.name, 'namespace_id': group_id})

    print('Creating labels')
    for label in labels:
        to_project.labels.create({
            'name': label.name,
            'color': label.color,
            'description': label.description,
            'priority': label.priority,
        })
        print(f'    Created {label.name}')

    print(f'Starting Copying...')
    for issue in issues:
        print(f'   Copying issue {issue.id}: {issue.title}... ', end='', flush=True)

        while True:
            try:
                print(f'Issue {issue.id}  - state: {issue.state}')

                # Create a new issue
                new_issue = to_project.issues.create({'title': issue.title, 'description': issue.description, 'state':issue.state, 'labels': issue.labels})
                if (issue.state == 'closed'):
                    new_issue.state_event = 'close'
                    new_issue.save()
                
                # Add notes to new issue
                for note in issue.notes.list():
                    print(f'Note ID: {note.id}')
                    print(f'body: {note.body}')
                    print(f'author: {note.author}')
                    print(f'created at: {note.created_at}')
                    new_issue.notes.create({'body':note.body, 'author': note.author, 'created_at':note.created_at})

            except OSError as e:
                print(f'\nError encountered ({e}). Retrying... ', end='', flush=True)
            except gitlab.GitlabUpdateError as e:
                if e.response_code == 400:
                    print(
                        f"\nUpdate error, insufficient permissions. Skipping this issue as this "
                        f"normally means the issue has already been moved."
                    )
                    break
                else:
                    raise
            else:
                break

        print('done')
    print('Copy complete!')

if __name__ == '__main__':
    main()
